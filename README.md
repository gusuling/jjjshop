### 三勾商城小程、支持多端发布，一套代码发布到8个平台，面向开发，方便二次开发


**项目介绍** 

三勾小程序商城基于thinkphp6+element-ui+uniapp打造的面向开发的小程序商城，方便二次开发或直接使用，可发布到多端，包括微信小程序、微信公众号、QQ小程序、支付宝小程序、字节跳动小程序、百度小程序、android端、ios端。


**项目演示** 

官网地址：http://www.jjjshop.net/      

后台演示：https://demo.jjjshop.net/shop     账号密码：admin/123456


 **如果对您有帮助，您可以点右上角 "Star" 支持一下，这样我们才有继续免费下去的动力，谢谢！
QQ交流群 (入群前，请在网页右上角点 "Star" )** 

交流QQ群：9512087  [点击加入](http://shang.qq.com/wpa/qunwpa?idkey=8be38c7c80b5a8fb311e9f01c1fe3e099d8ac59dce511e6c32fb44e33e054442)

 **扫码体验微信小程序** 

![输入图片说明](https://images.gitee.com/uploads/images/2020/0601/124128_0cb8eee9_1699189.jpeg "gh_a4bc432512c9_258.jpg")

 **小程序截图**

|  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0601/152028_cacd6623_1699189.jpeg "1.jpg")  |  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0601/152103_c6813475_1699189.jpeg "2.jpg") |   ![输入图片说明](https://images.gitee.com/uploads/images/2020/0601/152658_e93cde69_1699189.jpeg "3.jpg") |   ![输入图片说明](https://images.gitee.com/uploads/images/2020/0601/152710_6715bea5_1699189.jpeg "4.jpg")  |
|---|---|---|---|
|  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0601/152746_b172ecb3_1699189.jpeg "5.jpg")  |  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0601/152819_163da99e_1699189.jpeg "6.jpg") | ![输入图片说明](https://images.gitee.com/uploads/images/2020/0601/152829_8f9d617f_1699189.jpeg "7.jpg")  |   ![输入图片说明](https://images.gitee.com/uploads/images/2020/0601/152839_00673df9_1699189.jpeg "8.jpg") |
|  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0601/153101_c7dae4d2_1699189.jpeg "9.jpg") | ![输入图片说明](https://images.gitee.com/uploads/images/2020/0601/153112_efc06adb_1699189.jpeg "10.jpg")  |   |   |


 **后台截图** 

|![输入图片说明](https://images.gitee.com/uploads/images/2020/0601/153807_005336f6_1699189.png "1.png")   |  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0601/153819_72ba1c9f_1699189.png "2.png") |
|---|---|
| ![输入图片说明](https://images.gitee.com/uploads/images/2020/0601/153907_bd03e041_1699189.png "3.png")  | ![输入图片说明](https://images.gitee.com/uploads/images/2020/0601/153916_7f0afa3d_1699189.png "4.png")  |
|  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0601/153925_194bebf9_1699189.png "5.png") | ![输入图片说明](https://images.gitee.com/uploads/images/2020/0601/153935_cfdbd8f4_1699189.png "6.png")  |

 **软件架构**

后端：thinkphp6 管理端页面：element-ui 小程序端：uniapp。

部署环境建议：Linux + Nginx + PHP7.1-7.3 + MySQL5.6，上手建议直接用宝塔集成环境。

 **安装教程、开发文档、操作手册请进入官网查询** 

[官网链接](http://www.jjjshop.net)

 **bug反馈**

如果你发现了bug，请发送邮件到 bug@jiujiujia.net，我们将及时修复并更新。 

 **特别鸣谢** 
- thinkphp:[https://www.thinkphp.cn](https://www.thinkphp.cn)
- element-ui:[https://element.eleme.cn](https://element.eleme.cn)
- vue:[https://cn.vuejs.org/](https://cn.vuejs.org/)
- easywechat:[https://www.easywechat.com/](https://www.easywechat.com/)